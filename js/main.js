// Activamos los tooltip
$(function(){
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
});
// Retocamos la velocidad del Carrusel
$('#carouselExampleCaptions').carousel({
    interval:2000
});
// Ponemos el nombre del hotel en el Header del Modal
$('#exampleModal').on('show.bs.modal', function (e) {
    // Aviso evento
    console.info('modal abriendo. Si miras el código hay extras en esta acción!');
    // Cambiamos la propiedad del botón
    var button = $(e.relatedTarget);
    button.removeClass('btn-warning').addClass('btn-modal-show btn-danger disabled').prop('disabled', true);
    // EXTRA: AÑADIMOS el nombre del hotel al título del modal
    var hotel = button.data('hotel');
    var modal = $(this);
    modal.find('.modal-title').text('Reserva ' + hotel);
});
$('#exampleModal').on('shown.bs.modal', function (e) {
    console.info('modal abierto');
});
$('#exampleModal').on('hide.bs.modal', function (e) {
    console.info('modal ocultandose');
});
$('#exampleModal').on('hidden.bs.modal', function (e) {
    console.info('modal oculto');
    // Quitamos las propiedades al botón
    $('.btn-modal-show').removeClass('btn-modal-show btn-danger disabled').addClass('btn-warning').removeAttr('disabled');
});